const {series} 			= require('gulp')
const gulp            	= require('gulp')

const postcss         	= require('gulp-postcss')
const purgecss			= require('gulp-purgecss')
const autoprefixer 		= require('autoprefixer')





function purgeCSS() {
	return gulp
		.src('./dist/assets/css/*.css')
		.pipe(
			purgecss({
				content: [
					'./*.php',
					'./*.html',
					'./include/*.php',
					'./src/*.html',
					'./catering/*.php'
				],
				safelist: [
					// Bootstrap collapse element
					'collapsing',

					// Bootstrap carousel
					'carousel-fade', 'active', 'carousel-item-start', 'carousel-item-end', 'carousel-item-next', 'carousel-item-prev',
					'page--loaded'
				]
			})
		)
		.pipe(gulp.dest('./dist/assets/css'));
}





function prefix() {
	return gulp
		.src('./dist/css/*.css')
		.pipe(postcss([ autoprefixer({
			// grid: 'no-autoplace'
		}) ]))
		.pipe(gulp.dest('./dist/css'));
}







// Series scripts
// exports.watch = series(copyImages, copySVGs, copyFonts, copyVideos);
exports.build = series(purgeCSS, prefix);
exports.build_nopurge = series(prefix);